**Memory**

This was my first project in javascript & I chose to do a concentration game built on Node.js, Express, & Socket.io. A
description of the game can be found here https://en.wikipedia.org/wiki/Concentration_(game)

**Features**

* Players are _completely_ oblivious to tile content until flipped
* Image & word tiles
* Unlimited players per game
* Chat window
* Custom avatars
* Custom games with configurable names, scores to win, & tiles

