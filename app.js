// Dependencies
const express = require('express');
const http = require('http');
const path = require('path');
const socketIO = require('socket.io');

const app = express();
const server = http.Server(app);
const io = socketIO(server);

var games = {
    list:[] //Synced with clients not in a game
};
app.set('port', 5000);
app.use('/static', express.static(__dirname + '/static'));

// Routing
app.get('/', function(request, response) {
    response.sendFile(path.join(__dirname, 'index.html'));
});
app.get('/admin', function(request, response) {
    response.sendFile(path.join(__dirname, 'admin.html'));
});

// Starts the server.
server.listen(5000, function() {
    createDefaultGame();
    console.log('Started server on port 5000');
});
function clearPlayers(game) {
    games[game].orderedPlayers = [];
    games[game].sockets.stored = [];
}
function createDefaultGame() {
    var data = 'Default';
    const defSlots=[
        "/static/aa-battery.svg",
        "/static/apple-watchblue.svg",
        "/static/armchair.svg",
        "/static/bad-pig.svg",
        "/static/barricade.svg",
        "/static/bell.svg",
        "/static/calculator.svg",
        "/static/calendar.svg",
        "/static/cap.svg",
        "/static/carrot.svg",
        "/static/cup.svg",
        "/static/cutter.svg",
        "/static/diploma.svg",
        "/static/envelope-2.svg",
        "/static/find.svg",
        "/static/folder.svg",
        "/static/folder-heart.svg",
        "/static/football.svg",
        "/static/football-helmet.svg",
        "/static/gibson-lespaulguitarbass.svg",
        "/static/gopro.svg",
        "/static/headphone-big.svg",
        "/static/jar.svg"
    ];
    for (var i = 0; i < defSlots.length; i++) {
        var t = defSlots[i];
        var choice = Math.floor(Math.random() * defSlots.length);
        defSlots[i] = defSlots[choice];
        defSlots[choice] = t;
    }
    games[data] = {
        sockets: {
            stored: [],
            emit(channel, ...data1) {
                for (var i = 0; i < this.stored.length; i++) {
                    this.stored[i].emit(channel, data1);
                }
            }
        },
        slots: defSlots,
        board: [], //synced with client
        boardInfo: [], //Not synced with client, stores all board data
        orderedPlayers: [], //An ordered list of players playing used to track turn rotation
        turn: 0, //The current turn represented by a player id
        scoreToWin: 5,
        creator: undefined,
        turnStart: false, //Tracks whether it is the start of the turn or not
        flipped: [], //Tracks which tiles are flipped
        waiting: false
    };
    games.list.push(data);
    setup(data);
}
function setup(game) {
    for(var i=0; i<games[game].orderedPlayers.length; i++)
        games[game].orderedPlayers[i][1]=0;
    games[game].flipped = [];
    games[game].turn = 0;
    games[game].turnStart = false;
    games[game].board = new Array(36);
    games[game].board.fill(true);
    games[game].boardInfo = new Array(36);
    setupSlots(game);
    games[game].sockets.emit('update',games[game].board);
}
function setupSlots(game){
    var slots = [];
    for(var i=0; i<games[game].slots.length; i++)
        slots[i]=games[game].slots[i];
    var temp = new Array(36);
    for (var i = 0; i < temp.length; i++)
        temp[i] = i;
    for (var i = 0; i < temp.length; i++) {
        var t = temp[i];
        var choice = Math.floor(Math.random() * temp.length);
        temp[i] = temp[choice];
        temp[choice] = t;
    }
    while (slots.length > 0) {
        var slot = slots.pop();
        games[game].boardInfo[temp.pop()] = slot;
        games[game].boardInfo[temp.pop()] = slot;
    }
}
// Add the WebSocket handlers
io.on('connection', function(socket) {
    var game;
    var name;
    var gameInstance;
    socket.emit('game list',games.list);
    socket.on('request game list', function () {
        socket.emit('game list', games.list)
    });
    socket.on('create game',function (data,toWin,slots) {
        data = data.replace(new RegExp(" ",'g'), "_");
        games[data] = {
            sockets:{
                stored:[],
                emit(channel,...data1){
                    for(let i=0; i< this.stored.length; i++){
                        this.stored[i].emit(channel,data1);
                    }
                }
            },
            slots:slots[1],
            board:[], //synced with client
            boardInfo:[], //Not synced with client, stores all board data
            orderedPlayers:[], //An ordered list of players playing used to track turn rotation
            turn:0, //The current turn represented by a player id
            scoreToWin:toWin,
            creator:socket.id,
            turnStart:false, //Tracks whether it is the start of the turn or not
            flipped:[], //Tracks which tiles are flipped
            waiting:false
        };
        games.list.push(data);
        setup(data);
        setInterval(function () {
            if(games[data] !==  undefined && games[data].orderedPlayers.length===0){
                delete games[data];
                for(let i=0; i<games.list.length; i++)
                    if(games.list[i]===data)
                        games.list.splice(i,1);
                io.sockets.emit('game list',games.list)
            }
        },60000);
        io.sockets.emit('game list',games.list);
    });
    socket.on('refresh',function (data) {
        data = data.replace(new RegExp(" ",'g'), "_");
        if(games[data].creator === socket.id) {
            games[data].sockets.emit('kick');
            clearPlayers(data);
            setup(data);
        }
    });
    socket.on('devare',function (data) {
        data = data.replace(new RegExp(" ",'g'), "_");
        if(games[data] !== undefined && games[data].creator === socket.id) {
            games[data].sockets.emit('kick');
            clearPlayers(data);
            delete games[data];
            for(var i=0; i<games.list.length; i++)
                if(games.list[i]===data)
                    games.list.splice(i,1);
            io.sockets.emit('game list', games.list);
        }
    });
    socket.on('custom game',function (data) {
        socket.emit('game list',[data]);
    });
    socket.on('player joined', function (temp,data) {
        if(games[temp] === undefined)
            return;
        if(data===''){
            socket.emit('alert','The name you entered is invalid');
            return
        }
        game=temp;
        gameInstance=games[game];
        for(var i=0; i<gameInstance.orderedPlayers.length; i++)
            if(gameInstance.orderedPlayers[0][0]===data) {
                socket.emit('alert','The name you entered is already in use');
                return;
            }
        name = data;
        gameInstance.orderedPlayers[gameInstance.orderedPlayers.length]=[name,0,'/static/avatar.png'];
        gameInstance.sockets.stored.push(socket);
        socket.emit('sync board', gameInstance.board);
        gameInstance.sockets.emit('chat',name+' has joined the game');
        gameInstance.sockets.emit('sync players',gameInstance.orderedPlayers);
        socket.emit('preload',gameInstance.boardInfo);
        console.log(data + ' has joined the game');
    });
    socket.on('board clicked', function (data) {
        if (gameInstance !== undefined &&
            !gameInstance.waiting &&
            gameInstance.board[data] &&
            gameInstance.orderedPlayers[gameInstance.turn]!==undefined &&
            gameInstance.orderedPlayers[gameInstance.turn][0] === name) {
            gameInstance.sockets.emit('display', data, gameInstance.boardInfo[data]);
            gameInstance.turnStart = !gameInstance.turnStart;
            if (!gameInstance.turnStart) {
                if(gameInstance.flipped[0]!==data) {
                    gameInstance.flipped[1] = data;
                    gameInstance.waiting = true;
                    setTimeout(function () {
                        if (gameInstance.boardInfo[gameInstance.flipped[0]] === gameInstance.boardInfo[gameInstance.flipped[1]]) {
                            gameInstance.board[gameInstance.flipped[0]] = false;
                            gameInstance.board[gameInstance.flipped[1]] = false;
                            gameInstance.sockets.emit('player scored');
                            gameInstance.sockets.emit('disable', gameInstance.flipped[0]);
                            gameInstance.sockets.emit('disable', gameInstance.flipped[1]);
                            ++gameInstance.orderedPlayers[gameInstance.turn][1];
                            if (gameInstance.orderedPlayers[gameInstance.turn][1] >= gameInstance.scoreToWin) {
                                console.log(gameInstance.orderedPlayers[gameInstance.turn][0] + ' has won the game!');
                                name = null;
                                gameInstance.sockets.emit('player won');
                                gameInstance.waiting = true;
                                delete games[game];
                                for (var i = 0; i < games.list.length; i++)
                                    if (games.list[i] === game)
                                        games.list.splice(i, 1);
                                if (game === 'Default')
                                    createDefaultGame();
                                io.sockets.emit('game list', games.list);
                                game = undefined;
                                gameInstance = undefined;
                                return;
                            }
                            else {
                                var count=0;
                                for(var i=0; i<games[game].board.length; i++)
                                    if(games[game].board[i])
                                        count++;
                                if(count===0){
                                    setupSlots(game);
                                }
                            }
                            gameInstance.waiting = false;
                            return;
                        } else {
                            gameInstance.sockets.emit('display', gameInstance.flipped[0], '?');
                            gameInstance.sockets.emit('display', gameInstance.flipped[1], '?');
                        }
                        if ((gameInstance.turn + 1) >= gameInstance.orderedPlayers.length)
                            gameInstance.turn = 0;
                        else
                            gameInstance.turn++;
                        gameInstance.sockets.emit('turn change', gameInstance.turn);
                        gameInstance.waiting = false;
                    }, 1000)
                }
            } else {
                gameInstance.flipped[0] = data;
            }
        }
    });
    socket.on('chat',function (data) {
        if(gameInstance!==undefined)
            gameInstance.sockets.emit('chat',name+' > '+data);
    });
    socket.on('pfp update',function (data) {
        if(gameInstance!==undefined) {
            for (var i = 0; i < gameInstance.orderedPlayers.length; i++)
                if (gameInstance.orderedPlayers[i][0] === name) {
                    gameInstance.orderedPlayers[i][2] = data;
                    gameInstance.sockets.emit('pfp update', [i, data]);
                    return;
                }
        }
    });
    socket.on('leave game',function () {
        if(gameInstance !== undefined) {
            console.log(name + " has left the game");
            for(var i=0; i<gameInstance.sockets.stored.length; i++)
                if(gameInstance.sockets.stored[i]===socket)
                    gameInstance.sockets.stored.splice(i,1);
            for (var i = 0; i < gameInstance.orderedPlayers.length; i++)
                if (gameInstance.orderedPlayers[i][0] === name) {
                    gameInstance.orderedPlayers.splice(i, 1);
                    if (i === gameInstance.turn) {
                        gameInstance.turn = gameInstance.turn + 1 >= gameInstance.orderedPlayers.length ? 0 : gameInstance.turn + 1;
                        gameInstance.sockets.emit('turn change',gameInstance.turn);
                    }
                    gameInstance.sockets.emit('player leave', i);
                    gameInstance.sockets.emit('chat',name+" has left the game");
                    return;
                }
        }
    });
    socket.on('change name',function (data) {
        for(var i=0; i<gameInstance.orderedPlayers.length; i++)
            if(gameInstance.orderedPlayers[i][0]===name)
                gameInstance.orderedPlayers[i][0]=data;
        gameInstance.sockets.emit('sync players',gameInstance.orderedPlayers);
        name=data;
    });
    socket.on('disconnect',function () {
        if(gameInstance !== undefined) {
            console.log(name + " has left the game");
            for (var i = 0; i < gameInstance.orderedPlayers.length; i++)
                if (gameInstance.orderedPlayers[i][0] === name) {
                    gameInstance.orderedPlayers.splice(i, 1);
                    if (i === gameInstance.turn)
                        gameInstance.turn = gameInstance.turn + 1 >= gameInstance.orderedPlayers.length ? 0 : gameInstance.turn + 1;
                    gameInstance.sockets.emit('player leave', i);
                    gameInstance.sockets.emit('chat',name+" has left the game");
                    return;
                }
        }
    });
});